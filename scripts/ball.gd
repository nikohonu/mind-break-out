extends KinematicBody2D

var speed = 125
var velocity = Vector2()
var is_start = false


func start(rotation):
	velocity = Vector2(speed, 0).rotated(deg2rad(rotation))


func _process(delta):
	if Input.is_action_just_released("click") and not is_start:
		is_start = true
		start(-90)


func _physics_process(delta):
	if is_start:
		var collision = move_and_collide(velocity * delta)
		if collision:
			velocity = velocity.bounce(collision.normal)
			if collision.collider.get_groups() == ["Bricks"]:
				collision.collider.queue_free()
	else:
		position.x = get_node("../Paddle").position.x


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
	get_tree().quit()
