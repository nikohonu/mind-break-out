extends KinematicBody2D


func _ready():
	pass


func _process(delta):
	var mouse_x = get_viewport().get_mouse_position().x
	position.x = clamp(mouse_x, 0 + 16, 320 - 16)
